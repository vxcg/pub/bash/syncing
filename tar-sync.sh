#!/bin/bash

# AUTHORS: Venkatesh Choppella and Bodhisattva Burman Roy
# 2013-06-21 Suraj Samal
#  - Added ability to create archives dir if not present
#Usage: cd $dest; ./tar-sync.sh

# creates a .tgz file of the sync directory in the current
# archives directory if the .tgz does not already exist.
# The tgz file is named
# user@hostname-yyyy-mm-ddThh-mm-ss.tgz

# The user, hostname and timestamp are picked up from the
# last syncing operation.  

# see [[file:sync.org]] for
# structure of the destination directory $dest in which this
# file resides.  

ts=`cat sync/log/timestamp | sed s/:/-/g`
echo "ts=$ts"
us=`cat sync/log/username`
echo "us=$us"
ho=`cat sync/log/hostname`
echo "ho=$ho"

tarfile=$us'@'$ho-on-$ts.tgz
echo "tarfile=$tarfile"

if [ ! -d archives ]
then
 echo "mkdir archives"
 mkdir archives
fi

if [ ! -e archives/$tarfile ]
then
   echo "/bin/tar -cvzf $tarfile sync"
   /bin/tar -cvzf $tarfile sync
   echo "mv $tarfile archives"
   mv $tarfile archives
fi
