# Makefile driver for syncing, restoring across a server or
# locally
# Authors: Venkatesh Choppella, Bodhisattva Burman Roy
# 2013-06-25  Suraj
#    - Added targets restore-from-phd, archive-phd
#    - Modified sync-phd target to sync to appropiate direcory(/media/choppell/SYNC)
#    - Modified restore target  and removed flags (--delete, --delete-before) 
#	so as to not to delete extra files at home direcory during a restore

# Usage: 
#   make -k src=<src> dest=<dest> rhost=<rhost>  <target>

# where

#     - src :: denotes the src which is rsynced  (default ${test}/src)
#     - dest :: denotes the destination of the rsync (default  ${test}/dest)
#     - rhost :: denotes the colon-suffixed machine on which the destination is a directory (default empty)

# The script assumes the user names on the remote machine and the local are the same.


# make sync : for testing stuff from the following structure
#                ${HOME}/scratch/test-sync/
#                     src/
#                     dest/

# make -k remote=remote.iiit.ac.in:  : syncs ${HOME} with
# (note the colon suffix)              $remote/SYNC.  



# TODO:

# 1. The script assumes that the usernames on the remote and
#    local server are the same.  Fix this.
# Possible fix:  Nothing more than defining the variable
#    remote as user@remote.machine.com: 
# is needed.   Someone, Please test this!

# 2. The script is untested for restoring the archive. 
#  Tested the restore-to-usb target.   But the whole process
#  of restoring to a newly installed machine needs to be
#  tested thoroughly, particularly about what to do with
#  existing dot files.   The --update rsync flag in takes
#  care of keeping the newer targets on the destination
#  intact (or so I think). 



sync_log_dir=/tmp/${USER}-sync-log
sync_log_file=${sync_log_dir}/log-file
sync_log_timestamp=${sync_log_dir}/timestamp
sync_log_username=${sync_log_dir}/username
sync_log_hostname=${sync_log_dir}/hostname

username=${USER}
hostname=`hostname`
#remote host
rhost=
test=${HOME}/tmp/test-sync
#-----------------------------+
# user configurable variables |
#-----------------------------+------------------+
remote=pascal.iiit.ac.in:
src=${test}/src
# dest is a directory
# that could be either absolute or relative
# dest's default is set up for testing
dest=${test}/dest
phd=/media/choppell/Drive-2/SYNC
exclude-from=${HOME}/sys/bin/syncing/EXCLUDE
#------------------------------------------------+


archive-cmd=tar-sync.sh

archives=${dest}/archives
# sync could be a remote 
#deleted sibling of mirror
deleted=../DELETED

# pass this option if you want a dry run 
# dry=-n

# default is a real run. 
dry=

all: 
	echo "please choose a target explicitly"

echo:
	(echo "user=${user}"; echo "src=${src}"; echo "sync=${sync}")


sync: 
	(mkdir -p ${sync_log_dir};  \
	/usr/bin/rsync -avz   ${dry} --log-file=${sync_log_file} \
         --modify-window=1  --delete --backup \
         --backup-dir=${deleted} \
	 --exclude-from=${exclude-from} \
         ${src}/  \
        ${rhost}${dest}/sync/MIRROR; \
	tail -1 ${sync_log_file} | awk '{print $$1"T"$$2}'|sed 's/\//-/g' > ${sync_log_timestamp}; \
	echo ${username} > ${sync_log_username}; \
	echo ${hostname} > ${sync_log_hostname}; \
	/usr/bin/rsync -avz ${dry} ${sync_log_dir}/* ${rhost}${dest}/sync/log/; \
	\rm -rf ${sync_log_dir})

sync-new: 
	  (mkdir -p ${sync_log_dir};  \
	  	 /usr/bin/rsync -avz   ${dry} --log-file=${sync_log_file} \
		--modify-window=1  \
		${src}/  \
		${dest}; \
		tail -1 ${sync_log_file} | awk '{print $$1"T"$$2}'|sed 's/\//-/g' > ${sync_log_timestamp}; \
		echo ${username} > ${sync_log_username}; \
		echo ${hostname} > ${sync_log_hostname}; \
		/usr/bin/rsync -avz ${dry} ${sync_log_dir}/* ${dest}/sync/log/)




new:
	(echo "new"; make -k  src=/media/choppell/Ubuntu13.04/home/choppell dest=/home/choppell sync-new)


dry-new:
	(echo "new"; make -k dry=-n src=/media/choppell/Ubuntu13.04/home/choppell dest=/home/choppell sync-new)

archive:
	${ssh} ${rhost}${dest}/tar-sync.sh



dry-sync: 
	(echo "dry-sync"; make -k dry=-n sync)


show-sync: 
	(echo "SHOW-SYNC"; make -nk sync)

sync-remote:
	(echo "SYNC-REMOTE"; make -k  src=${HOME}  dest=SYNC rhost=${remote} sync)

sync-phd:
	(echo "SYNC-PHD"; make -k  src=${HOME}  dest=${phd} sync)

show-sync-phd:
	(echo "SHOW-SYNC-PHD"; make -nk  src=${HOME}  dest=${phd}  sync)


sync-per:
	(echo "SYNC-PER"; make -k  src=/media/choppell/personal/per  dest=/media/choppell/Drive-1/backups/personal/per sync)

show-sync-per:
	(echo "SHOW-SYNC-PER"; make -nk  src=/media/choppell/personal/per  dest=/media/choppell/Drive-1/backups/personal/per sync)


show-sync-remote:
	(echo "SHOW-SYNC-REMOTE"; make -nk  src=${HOME} dest=SYNC rhost=${remote} sync)

dry-sync-remote:
	(echo "SHOW-SYNC-REMOTE"; make -k dry=-n  src=${HOME} dest=SYNC rhost=${remote} sync)


restore: 
	(mkdir -p ${sync_log_dir};  \
	/usr/bin/rsync  ${dry} -av  --update --log-file=${sync_log_file} \
         --modify-window=1  ${rhost}${src}/  ${dest}; \
	tail -1 ${sync_log_file} | awk '{print $$1"T"$$2}'|sed 's/\//-/g' > ${sync_log_timestamp}; \
	echo ${username} > ${sync_log_username}; \
	echo ${hostname} > ${sync_log_hostname}; )

restore-from-phd:
	(echo "restore-from-phd"; make restore src=${phd}/sync/MIRROR dest=${HOME})

archive-phd:
	(echo "TAR-SYNC-PHD"; \
        cd ${phd}/; ${PWD}/${archive-cmd};)



